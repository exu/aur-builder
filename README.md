This container is intended to simplify automated build environments of AUR packages.  
It currently supports native compilation for x86_64 and arm64. **No cross-compilation is possible** (yet)

The container is single use only, so some automation around creating the container, getting the result and destroying it is required.

# TODO

- [ ] Implement package signing
- [ ] Handle AUR dependencies
- [ ] Enable cross-compilation

# Usage

To use this container, simply provide the name of an AUR package after the image  
Make sure to mount `/out` inside the container to a directory on your host, to receive the package output  
`podman run -v <host path>:/out gitea.exu.li/exu/aur-builder:latest <package>`

# Building

Example for single build  
`buildah build -f builder-x86.dockerfile -t builderx86:latest`

Build all containers and push by executing `create-containers.sh`
