FROM docker.io/menci/archlinuxarm:base-devel

# install dependencies
RUN pacman -Syu --noconfirm \
    sudo \
    git

# allow sudo for all users
RUN echo '%wheel ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/wheel

# add builder user
RUN useradd builder -s /bin/bash -G wheel

# copy builder script
COPY --chmod=755 builder.sh /

# execute builder script
ENTRYPOINT [ "/builder.sh" ]
