#!/usr/bin/env sh
set -euo pipefail

# check if one argument is present
if [ "$#" -ne 1 ]; then
    echo "Pass the AUR package name you want to build"
    exit 1
fi

# relaunch script as builder user
if [ "$(whoami)" != "builder" ]; then
    sudo -u builder bash "$0" "$@"
    exit
fi

echo "MAKEFLAGS=\"-j$(nproc)\"" | sudo tee -a /etc/makepkg.conf

export TMPDIR=$(mktemp -d)

cd "$TMPDIR"

git clone https://aur.archlinux.org/"$1".git

cd "$TMPDIR/$1"

makepkg -As --noconfirm

sudo mkdir -p /out

sudo cp ./*".pkg.tar."* /out
