#!/usr/bin/env sh

# read variables from file
eval "$(cat variables)"

# Create a multi-architecture manifest
buildah manifest create ${MANIFEST_NAME}

# Build your amd64 architecture container
buildah bud \
    --tag "${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}" \
    --tag "${REGISTRY}/${USER}/${IMAGE_NAME}:latest" \
    --manifest ${MANIFEST_NAME} \
    --file ${AMD64_FILE} \
    --arch amd64

# Build your arm64 architecture container
buildah bud \
    --tag "${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}" \
    --tag "${REGISTRY}/${USER}/${IMAGE_NAME}:latest" \
    --manifest ${MANIFEST_NAME} \
    --file ${ARM64_FILE} \
    --arch arm64

# Push the full manifest, with both CPU Architectures
buildah manifest push --all \
    ${MANIFEST_NAME} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
    ${MANIFEST_NAME} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:latest"

# remove manifest
buildah manifest rm ${MANIFEST_NAME}
